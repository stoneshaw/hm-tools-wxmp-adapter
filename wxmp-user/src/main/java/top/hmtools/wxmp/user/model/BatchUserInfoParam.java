package top.hmtools.wxmp.user.model;

import java.util.List;

public class BatchUserInfoParam {

	private List<UserInfoParam> user_list;

	public List<UserInfoParam> getUser_list() {
		return user_list;
	}

	public void setUser_list(List<UserInfoParam> user_list) {
		this.user_list = user_list;
	}
	
	
}
