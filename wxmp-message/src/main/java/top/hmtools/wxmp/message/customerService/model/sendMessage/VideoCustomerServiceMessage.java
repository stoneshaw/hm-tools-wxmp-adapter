package top.hmtools.wxmp.message.customerService.model.sendMessage;

import top.hmtools.wxmp.message.customerService.model.BaseSendMessageParam;

/**
 * 消息管理--客服消息--发送视频消息
 * @author HyboWork
 *
 */
public class VideoCustomerServiceMessage extends BaseSendMessageParam{

	private Video video;

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	@Override
	public String toString() {
		return "VideoCustomerServiceMessage [video=" + video + ", touser=" + touser + ", msgtype=" + msgtype + "]";
	}
	
	
}
