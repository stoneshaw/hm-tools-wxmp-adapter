package top.hmtools.wxmp.message.customerService.model.sendMessage;

import top.hmtools.wxmp.message.customerService.model.BaseSendMessageParam;

/**
 * 消息管理 之 客服消息 之 语音消息
 * @author hybo
 *
 */
public class VoiceCustomerServiceMessage extends BaseSendMessageParam {

	/**
	 * 语音消息
	 */
	private MediaBean voice;

	public MediaBean getVoice() {
		return voice;
	}

	public void setVoice(MediaBean voice) {
		this.voice = voice;
	}

	@Override
	public String toString() {
		return "VoiceCustomerServiceMessage [voice=" + voice + ", touser=" + touser + ", msgtype=" + msgtype + "]";
	}

	
	
}
