package top.hmtools.wxmp.menu.models.eventMessage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseEventMessage;
import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * pic_photo_or_album：弹出拍照或者相册发图的事件推送
 * <br>
 * <p>
 * <xml>
	<ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName>
	<FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName>
	<CreateTime>1408090816</CreateTime>
	<MsgType><![CDATA[event]]></MsgType>
	<Event><![CDATA[pic_photo_or_album]]></Event>
	<EventKey><![CDATA[6]]></EventKey>
	<SendPicsInfo>
		<Count>1</Count>
		<PicList>
			<item>
				<PicMd5Sum><![CDATA[5a75aaca956d97be686719218f275c6b]]></PicMd5Sum>
			</item>
		</PicList>
	</SendPicsInfo>
</xml>
 * </p>
 * @author Hybomyth
 *
 */
@WxmpMessage(msgType=MsgType.event,event=Event.pic_photo_or_album)
public class PicPhotoOrAlbumEventMessage extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -193956450871541264L;

	/**
	 * 发送的图片信息
	 */
	@XStreamAlias("SendPicsInfo")
	private SendPicsInfo sendPicsInfo;

	/**
	 * 发送的图片信息
	 * @return
	 */
	public SendPicsInfo getSendPicsInfo() {
		return sendPicsInfo;
	}

	/**
	 * 发送的图片信息
	 * @param sendPicsInfo
	 */
	public void setSendPicsInfo(SendPicsInfo sendPicsInfo) {
		this.sendPicsInfo = sendPicsInfo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void configXStream(XStream xStream) {
		xStream.alias("item", SendPicItem.class);
	}

}
